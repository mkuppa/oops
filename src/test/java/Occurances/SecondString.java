package Occurances;

public class SecondString extends FirstString {
    static int counter;
    static String tempString = "";

    public static void findingOccurences(String str1, String str2) {
        int lengthOfString2 = str2.length();
        str1 = str1.toLowerCase();
        str2 = str2.toLowerCase();
        while (counter < lengthOfString2) {
            char c = str2.charAt(counter);
            str1 = removeOccurences(str1, c);
            counter++;
        }
        System.out.println("Final:" + str1);
    }

}
