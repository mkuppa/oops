package PasswordGenerator;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class Generator {

    static String password = "";
    static HashMap<Integer, Character> map = new HashMap<>();

    public static void setHashMap() {
        int x = 97;
        for (int go = 0; go < 10; go++) {
            map.put((go), (char) (x + go));
        }
    }

    public static String convert(String j) {
        setHashMap();
        for (int len = 0; len < j.length(); len++) {
            password += map.get(Integer.parseInt(String.valueOf(j.charAt(len))));
        }
        System.out.println("New Generated Password is : " + password);
        return password;
    }
}
